////////////////////////// SECTIONS
	$('#listsections').live('pageshow', function (event, ui) {
		$.mobile.loading('show');
		$.ajax({
	  	url: "http://prowebdesigns.org:3000/sections?course_id=1000"
		}).done(function ( data ) {
		  var coursename = data[0]['section']['course']['title'];
		  $('#coursename').text(coursename);
			addSectionDatatoList(data);
		});
  });

function addSectionDatatoList (data) {
	for(var i = 0; i < data.length; i++) {
    	var sectionNum = data[i]['section']['sectionNum'];
    	var instructorname = data[i]['section']['instructor']['lastname'] + ", " + data[i]['section']['instructor']['firstname'];
    	var days = data[i]['section']['days'];
    	var listitemhead = $('<h3>').text("00" + sectionNum).addClass('sectionNumber');
    	var instructorline = $('<h3>').text(instructorname);
    	var times = data[i]['section']['prettyStartTime'] + " - " + data[i]['section']['prettyEndTime'];
    	var listitempara = $('<p>').html(days + " " + times);
			var link = $('<a>').append(listitemhead).append(instructorline).append(listitempara);
			var listitem = $('<li>').append(link).addClass('sectionlistitem');
    	$('#sectionlist').append(listitem);
 	}
	$('#sectionlist').listview('refresh');
	$.mobile.loading('hide');
}

///////////////////////////////////////////////////////////////
////////////////  COURSES  //////////////////////////////////////
///////////////////////////////////////////////////////////////
	$('#courses').live('pageshow', function (event, ui) {
		$.mobile.loading('show');
		$.ajax({
	  	url: "http://prowebdesigns.org:3000/courses.json?page=1"
		}).done(function ( data ) {
			addCourseDatatoList(data);
		});
		
		var page = 1;
		$(window).scroll(function()
		{
				var nearToBottom = 100;
				//if($(window).scrollTop() + $(window).height() > $(document).height() - 75)
				//if($(window).scrollTop() == $(document).height() - $(window).height())
				if ($(window).scrollTop() + $(window).height() > $(document).height() - nearToBottom)
		    {
							page += 1;
		        	$.ajax({
		        		url: "http://prowebdesigns.org:3000/courses.json?page=" + page
							}).done(function ( data ) {
								addCourseDatatoList(data);
		        	});
		    }
		});
  });

function addCourseDatatoList (data) {
	for(var i = 0; i < data.length; i++) {
    	var coursetitle = data[i]['course']['title'];
			var deptid = data[i]['course']['department_id'];
			var deptname = data[i]['course']['department']['name'];
    	//console.log(coursetitle);
			var link = $('<a>').text(coursetitle);
			var listitem = $('<li>').attr({'data-department-id':deptid, 'data-department-name':deptname}).append(link);
    	$('#courselist').append(listitem);
 	}
	//$('#courselist').listview('refresh');
	$('#courselist').listview({
			autodividers : true,
			autodividersSelector: function(li) {
				var out = li.attr('data-department-name');
				console.log(out);
				return out;
			}
		}).listview('refresh');
	$.mobile.loading('hide');
}
