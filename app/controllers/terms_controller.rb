class TermsController < ApplicationController
  # GET /terms
  # GET /terms.json
  def index
    @terms = Term.all

    render json: @terms
  end

  # GET /terms/1
  # GET /terms/1.json
  def show
    @term = Term.find(params[:id])

    render json: @term
  end

  # GET /terms/new
  # GET /terms/new.json
  def new
    @term = Term.new

    render json: @term
  end

  # POST /terms
  # POST /terms.json
  def create
    @term = Term.new(params[:term])

    if @term.save
      render json: @term, status: :created, location: @term
    else
      render json: @term.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /terms/1
  # PATCH/PUT /terms/1.json
  def update
    @term = Term.find(params[:id])

    if @term.update_attributes(params[:term])
      head :no_content
    else
      render json: @term.errors, status: :unprocessable_entity
    end
  end

  # DELETE /terms/1
  # DELETE /terms/1.json
  def destroy
    @term = Term.find(params[:id])
    @term.destroy

    head :no_content
  end
end
