class ApplicationController < ActionController::API
	after_filter :set_access_control_headers
	def set_access_control_headers
		headers['Access-Control-Allow-Origin'] = '*'
		headers['Access-Control-Request-Method'] = '*'
		#headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE"
	end

helper_method :current_student

private

def current_student
  @current_student ||= Student.find(session[:student_id]) if session[:student_id]
end
end
