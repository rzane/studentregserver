class StudentsController < ApplicationController
  # GET /students
  # GET /students.json
  def index
    @students = Student.all

    render json: @students
  end

  # GET /students/1
  # GET /students/1.json
  def show
    @student = Student.find(params[:id])

    render json: @student
  end

  # GET /students/new
  # GET /students/new.json
  def new
    @student = Student.new

    render json: @student
  end

  # POST /students
  # POST /students.json
  def create
		incomingstudent = params[:student]
		
		parsed = ActiveSupport::JSON.decode(incomingstudent)
		newstudent = {
			"firstname" => parsed['firstname'],
			"lastname" => parsed['lastname'],
			"email" => parsed['email'],
			"password" => parsed['password'],
			"regtime_id" => parsed['regtime_id'],
			"studentIDnumber" => parsed['studentIDnumber']
		}
    
		@student = Student.new(newstudent)
    if @student.save
      render json: @student, status: :created, location: @student
    else
      render json: @student.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    @student = Student.find(params[:id])

    if @student.update_attributes(params[:student])
      head :no_content
    else
      render json: @student.errors, status: :unprocessable_entity
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student = Student.find(params[:id])
    @student.destroy

    head :no_content
  end
end
