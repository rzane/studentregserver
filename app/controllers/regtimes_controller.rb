class RegtimesController < ApplicationController
  # GET /regtimes
  # GET /regtimes.json
  def index
    @regtimes = Regtime.order('id DESC')

    render json: @regtimes
  end

  # GET /regtimes/1
  # GET /regtimes/1.json
  def show
    @regtime = Regtime.find(params[:id])
    render json: @regtime.to_json(:methods => :prettyTime)

    #render json: @regtime
  end

  # GET /regtimes/new
  # GET /regtimes/new.json
  def new
    @regtime = Regtime.new

    render json: @regtime
  end

  # POST /regtimes
  # POST /regtimes.json
  def create
    @regtime = Regtime.new(params[:regtime])

    if @regtime.save
      render json: @regtime, status: :created, location: @regtime
    else
      render json: @regtime.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /regtimes/1
  # PATCH/PUT /regtimes/1.json
  def update
    @regtime = Regtime.find(params[:id])

    if @regtime.update_attributes(params[:regtime])
      head :no_content
    else
      render json: @regtime.errors, status: :unprocessable_entity
    end
  end

  # DELETE /regtimes/1
  # DELETE /regtimes/1.json
  def destroy
    @regtime = Regtime.find(params[:id])
    @regtime.destroy

    head :no_content
  end
end
