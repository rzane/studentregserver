class SectionsController < ApplicationController
  # GET /sections
  # GET /sections.json
	def getfromregistrations
		registrations = params[:regs]
		json = ActiveSupport::JSON.decode(registrations)
		registrationstofind = Array.new
		regs = json['registrations']
		regs.each do |reg|
			registrationstofind.push(regs)
		end
		
		registrations = Registration.where(:id => registrationstofind)

		render json: registrations.to_json(:include => [:section, :course])
	end

  def index
    #@sections = Section.all
		
		@sections = Section.where(:course_id => params[:course_id])
		#render json: @sections
		render json: @sections.to_json(:include => [:instructor, :course])
  end

  # GET /sections/1
  # GET /sections/1.json
  def show
    @section = Section.find(params[:id])

    render json: @section.to_json(:include => [:instructor, :course, :department])
  end

  # GET /sections/new
  # GET /sections/new.json
  def new
    @section = Section.new

    render json: @section
  end

  # POST /sections
  # POST /sections.json
  def create
    @section = Section.new(params[:section])

    if @section.save
      render json: @section, status: :created, location: @section
    else
      render json: @section.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /sections/1
  # PATCH/PUT /sections/1.json
  def update
    @section = Section.find(params[:id])

    if @section.update_attributes(params[:section])
      head :no_content
    else
      render json: @section.errors, status: :unprocessable_entity
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section = Section.find(params[:id])
    @section.destroy

    head :no_content
  end
end
