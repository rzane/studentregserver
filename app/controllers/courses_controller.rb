class CoursesController < ApplicationController
  # GET /courses
  # GET /courses.json
  def index
    @courses = Course.page(params[:page]).per(40)

    render json: @courses.to_json(:include => [:department], :methods => :genHTML)
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
    #@course = Course.find(params[:id])
    @courses = Course.order("title asc").where(:department_id => params['deptID'])
    render json: @courses.to_json(:include => [:department])
  end

  def search
	@courses = Course.where("title like ?", "%#{params[:query]}%")
	htmlstring = ""
    	@courses.each do |course|
        	@dept = Department.find(course.department_id)
        	htmlstring += <<-EOS
<li data-course-id="#{course.id}" data-department-id="#{course.department_id}" data-department-name="#{@dept.name}" class="courselistitem">
        <a href="#">#{course.title}</a></li>		
EOS

    	end
    	jsonstring = {"courses" => {"page" => "#{params[:page]}", "query" => "#{params[:query]}", "htmlstring" => "#{htmlstring}"}}
        render json: jsonstring.to_json
  end

  def lazyload
    @courses = Course.page(params[:page]).per(80)
    htmlstring = ""
    @courses.each do |course|
	@dept = Department.find(course.department_id)
	htmlstring += <<-EOS
<li data-course-id="#{course.id}" data-department-id="#{course.department_id}" data-department-name="#{@dept.name}" class="courselistitem">
        <a href="#">#{course.title}</a></li>
EOS

    end
    jsonstring = {"courses" => {"page" => "#{params[:page]}","htmlstring" => "#{htmlstring}"}}
	render json: jsonstring.to_json 

  end	  

  # GET /courses/new
  # GET /courses/new.json
  def new
    @course = Course.new

    render json: @course.to_json
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(params[:course])

    if @course.save
      render json: @course, status: :created, location: @course
    else
      render json: @course.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    @course = Course.find(params[:id])

    if @course.update_attributes(params[:course])
      head :no_content
    else
      render json: @course.errors, status: :unprocessable_entity
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course = Course.find(params[:id])
    @course.destroy

    head :no_content
  end
end
