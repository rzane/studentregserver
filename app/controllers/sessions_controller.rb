class SessionsController < ApplicationController
def new
end

def create
  student = Student.authenticate(params[:email], params[:password])
  if student
    session[:student_id] = student.id
    #redirect_to root_url, :notice => "Logged in!"
    render json: student
  else
    render :json => { :errors => 'Student Does Not Exist' }, :status => 422
    ##### MAKE THIS A JSON ERROR MESSAGE
  end
end

def destroy
  session[:student_id] = nil
  render :json => { :success => 'User Logged Out' }
end
end
