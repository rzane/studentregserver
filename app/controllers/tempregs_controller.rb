class TempregsController < ApplicationController
  # GET /tempregs
  # GET /tempregs.json
  def index
    @tempregs = Tempreg.all

    render json: @tempregs
  end

  # GET /tempregs/1
  # GET /tempregs/1.json
  def show
    @tempreg = Tempreg.find(params[:id])

    render json: @tempreg
  end

  # GET /tempregs/new
  # GET /tempregs/new.json
  def new
    @tempreg = Tempreg.new

    render json: @tempreg
  end

  # POST /tempregs
  # POST /tempregs.json
  def create
    @tempreg = Tempreg.new(params[:tempreg])

    if @tempreg.save
      render json: @tempreg, status: :created, location: @tempreg
    else
      render json: @tempreg.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tempregs/1
  # PATCH/PUT /tempregs/1.json
  def update
    @tempreg = Tempreg.find(params[:id])

    if @tempreg.update_attributes(params[:tempreg])
      head :no_content
    else
      render json: @tempreg.errors, status: :unprocessable_entity
    end
  end

  # DELETE /tempregs/1
  # DELETE /tempregs/1.json
  def destroy
    @tempreg = Tempreg.find(params[:id])
    @tempreg.destroy

    head :no_content
  end
end
