class RegistrationsController < ApplicationController
  # GET /registrations
  # GET /registrations.json
  def index
    student_id = params[:student_id]
    @registrations = Registration.where(:student_id => student_id)

    render json: @registrations.to_json(:include => [:section, :course])
  end

  # GET /registrations/1
  # GET /registrations/1.json
  def show
    @registration = Registration.find(params[:id])

    render json: @registration
  end

  def regtempregs
   registrations = Array.new
	 failed = Array.new
   tempregsstring = params[:tempregs]
   studentid = params[:student_id] 
   student = Student.find(studentid)
   if student
     parsed_json = ActiveSupport::JSON.decode(tempregsstring)
     regs = parsed_json
     regs.each do |reg|
       section = Section.find(reg)
       if section
       	@registration = Registration.new(:student_id => student.id, :section_id => section.id )
				if @registration.save
					registrations.push(@registration)
				else
         @section = Section.where(:id => section.id)
				 @course = Course.find(@section)
         failed.push(@course.title)
				end
       else
       	 render :json => {'errors' => 'section does not exist'}.to_json
       end
     end


   else
     render :json =>  {'errors'=>'student does not exist'}.to_json
   end

   	render json: {"registrations" => registrations, "failed" => failed}.to_json##registrations.to_json
  end


  # GET /registrations/new
  # GET /registrations/new.json
  def new
    @registration = Registration.new

    render json: @registration
  end

  # POST /registrations
  # POST /registrations.json
  def create
    @registration = Registration.new(params[:registration])

    if @registration.save
      render json: @registration, status: :created, location: @registration
    else
      render json: @registration.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /registrations/1
  # PATCH/PUT /registrations/1.json
  def update
    @registration = Registration.find(params[:id])

    if @registration.update_attributes(params[:registration])
      head :no_content
    else
      render json: @registration.errors, status: :unprocessable_entity
    end
  end

	def deletereg
		@registration = Registration.find(params[:id])
    @registration.destroy

    head :no_content
	end

  # DELETE /registrations/1
  # DELETE /registrations/1.json
  def destroy
    @registration = Registration.find(params[:id])
    @registration.destroy

    head :no_content
  end
end
