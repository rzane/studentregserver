class Department < ActiveRecord::Base
  attr_accessible :name
	
	has_many :courses
	has_many :instructors
	has_many :sections, :through => :courses
end
