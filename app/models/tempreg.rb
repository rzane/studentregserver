class Tempreg < ActiveRecord::Base
  attr_accessible :section_id, :student_id

	belongs_to :student
	belongs_to :section
end
