class Instructor < ActiveRecord::Base
  attr_accessible :department_id, :firstname, :lastname, :rmpUrl

	belongs_to :department
	has_many :sections
end
