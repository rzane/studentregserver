class Section < ActiveRecord::Base
  attr_accessible :sectionNum, :capacity, :course_id, :courseType, :crn, :days, :endDate, :endTime, :enrolled, :instructor_id, :location, :startDate, :startTime, :prettyStartTime, :prettyEndTime

	belongs_to :instructor
	belongs_to :course
	has_many :tempregs
	has_many :registrations
	has_one :department, :through => :course
end
