class Registration < ActiveRecord::Base
  attr_accessible :section_id, :student_id

  belongs_to :student
  belongs_to :section
  has_one :course, :through => :section
  validates :section_id, :uniqueness => {:scope => :student_id}

end
