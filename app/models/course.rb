class Course < ActiveRecord::Base
  attr_accessible :courseNum, :credits, :department_id, :subject, :term_id, :title
	
	has_many :sections
	belongs_to :department
	belongs_to :term

	attr_accessor :genHTML

	def genHTML
		litemplate = <<-EOS
<li data-department-id="#{self.department_id}" class="courselistitem">
	<a href="Section?course_id=#{self.id}">#{self.title}</a> 
</li>
EOS

		return litemplate
		#return self.title
	end

	def test(department)
		return department
	end
end
