class Regtime < ActiveRecord::Base
  attr_accessible :startTime, :studenttype, :term_id

	has_many :students
	belongs_to :term


	def prettyTime 
	  return self.startTime.strftime('%B %-d, %Y %k:%M:%S')
	end

end

