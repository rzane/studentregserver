namespace :db do
  desc "Add Sample Courses"
  task :createinstructors => :environment do
		require 'json'

		json = File.open("#{Rails.root}/public/wvucourses.json").read
		courses = JSON.parse(json)

		$i = 0
		$num = courses.length
		while $i < $num  do
			$fullname = courses[$i]['instructor']
			
			$lname = $fullname.split(", ")[0]
			$fname = $fullname.split(", ")[1]

			$dept = courses[$i]['dept']
			
			@department = Department.find_by_name($dept)
			$department_id = @department.id
			
			$rmpURL = "http://www.ratemyprofessors.com/ShowRatings.jsp?tid=" + rand(99999).to_s.center(5, rand(9).to_s)
			
			if Instructor.exists?(:firstname => $fname, :lastname => $lname, :department_id => $department_id)
				puts 'INSTRUCTOR ALREADY EXISTS'
			else
	      @instructor = Instructor.new(:firstname => $fname, :lastname => $lname, :rmpUrl => $rmpURL, :department_id => @department.id)
				if @instructor.save
        	puts "INSTRUCTOR: Name: #{$fname} #{$lname} \t Department: #{@department.name}"
				else
					puts 'ERROR LOADING INSTRUCTOR'
				end
			end
			$i +=1
		end
	end	
end

