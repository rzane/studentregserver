namespace :db do
  desc "Add Sample Courses"
  task :createcourses => :environment do
		require 'json'

		json = File.open("#{Rails.root}/public/wvucourses.json").read
		courses = JSON.parse(json)

		$i = 0
		$num = courses.length
		while $i < $num  do
			$dept = courses[$i]['dept']
			@department = Department.find_by_name($dept)

			$subject = courses[$i]['subject']
			$courseNum = courses[$i]['course']
			$title = courses[$i]['title']			
			$credits = courses[$i]['credits']
			@term = Term.find_by_name('Spring 2013')
			

			if Course.exists?(:subject => $subject, :courseNum => $courseNum)
				puts 'COURSE ALREADY EXISTS'
			else
				@course = Course.new(:subject => $subject, :courseNum => $courseNum, :title => $title, :credits => $credits, :department_id => @department.id, :term_id => @term.id)
				if @course.save
					puts "COURSE: Subject: #{$subject} \t CourseNum: #{$courseNum} \t Title: #{$title} \t Dept: #{@department.name} \t Term: #{@term.name}"
				else
					puts 'ERROR LOADING COURSE'
				end
			end
			$i +=1
		end
	end	
end
