namespace :db do
  desc "Add Sample Courses"
  task :createregtimes => :environment do
		@term = Term.find_by_name("Spring 2013")
		$termid = @term.id

		$studenttype = "Professional, Graduate, or Honors-SR"
		$startTime = "2013-04-02 [08:00:00]"
		createRegtime($studenttype, $startTime, $termid)

    $studenttype = "Honors-JR, Law-L2"
    $startTime = "2013-04-03 [08:00:00]"
    createRegtime($studenttype, $startTime, $termid)
		
    $studenttype = "Honors-SO, Law-L1"
    $startTime = "2013-04-04 [08:00:00]"
    createRegtime($studenttype, $startTime, $termid)

    $studenttype = "Honors-FR"
    $startTime = "2013-04-05 [08:00:00]"
    createRegtime($studenttype, $startTime, $termid)

    $studenttype = "Seniors"
    $startTime = "2013-04-08 [08:00:00]"
    createRegtime($studenttype, $startTime, $termid)

    $studenttype = "Juniors"
    $startTime = "2013-04-10 [08:00:00]"
    createRegtime($studenttype, $startTime, $termid)

    $studenttype = "Sophmores"
    $startTime = "2013-04-12 [08:00:00]"
    createRegtime($studenttype, $startTime, $termid)

    $studenttype = "Freshmen"
    $startTime = "2013-04-15 [08:00:00]"
    createRegtime($studenttype, $startTime, $termid)

	end

	def createRegtime(studenttype,startTime, termid)
		@regtime = Regtime.new(:studenttype => studenttype, :startTime => startTime, :term_id => termid)
		if @regtime.save
			puts "REGTIME: Student Type: #{studenttype} \t Start Time: #{startTime} \t Term: #{termid}"
		else
			puts "ERROR LOADING REGTIMES"
		end
	end
end
