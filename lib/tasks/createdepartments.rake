namespace :db do
  desc "Add Sample Courses"
  task :createdepartments => :environment do
		require 'faker'
		require 'json'

		json = File.open("#{Rails.root}/public/wvudepartments.json").read
		departments = JSON.parse(json)

		$i = 0
		$num = departments.length
		while $i < $num  do
			$name = departments[$i]['title']
			@dept = Department.new(:name => $name)
			if @dept.save
				puts "DEPARTMENT: Name: #{$name}"				
			else
				puts "ERROR LOADING DEPARTMENTS"
			end			
			$i +=1
		end

	end
end
