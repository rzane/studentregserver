namespace :db do
  desc "Add Sample Courses"
  task :createstudents => :environment do
		require 'faker'

		$i = 0
		$num = 25

		while $i < $num  do
			$fname = Faker::Name.first_name
			$lname = Faker::Name.last_name
			$student_id = rand(799999999).to_s.center(9, rand(9).to_s)
			$email = Faker::Internet.email
			$password = 'testpass'
			$searchable = rand(2)
			offset = rand(Regtime.count)
			@regtime = Regtime.first(:offset => offset)
			$regtime_id = @regtime.id
			@student = Student.new(:firstname => $fname, :lastname => $lname, :studentIDnumber => $student_id, :email => $email, :password => $password, :searchable => $searchable, :regtime_id => $regtime_id)
			if @student.save
				puts "STUDENT: Name: #{$fname} #{$lname} \t StudentID: #{$student_id} \t Email: #{$email} \t Reg Time: #{@regtime.studenttype}" 			
			else
				puts "ERROR LOADING STUDENTS"
			end
			$i += 1
		end

		Student.create(:firstname => "Ray", :lastname => "Zane", :studentIDnumber => 701110957, :email => "rzane@mix.wvu.edu", :password => "testpass", :searchable => 0, :regtime_id => 3)
	end
end
