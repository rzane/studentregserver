namespace :db do
  desc "Add Sample Courses"
  task :createterms => :environment do
		$termname = "Spring 2013"
		createTerm($termname)

		$termname = "Fall 2012"
		createTerm($termname)
	end

	def createTerm(name)
		@term = Term.new(:name => name)
		if @term.save
			puts "TERM: Name: #{name}"
		else
			puts "ERROR LOADING TERMS"
		end
	end
end
