namespace :db do
  desc "Add Sample Sections"
  task :createsections => :environment do
		require 'faker'
		require 'json'
		require 'time'

		json = File.open("#{Rails.root}/public/wvucourses.json").read
		sections = JSON.parse(json)

		$i = 0
		$num = sections.length
		while $i < $num  do
			$subject = sections[$i]['subject']
			$courseNum = sections[$i]['course']
			@course = Course.find_by_subject_and_courseNum($subject, $courseNum)

		      	$instructorfull = sections[$i]['instructor']
			
      			$lname = $instructorfull.split(", ")[0]
      			$fname = $instructorfull.split(", ")[1]
			
			$dept = sections[$i]['dept']
			@department = Department.find_by_name($dept)

			@instructor = Instructor.where(:firstname => $fname, :lastname => $lname, :department_id => @department.id).first
			$instructor_id = @instructor.id
			
			$section = sections[$i]['section']
			$crn = sections[$i]['crn']
			$courseType = sections[$i]['type']
			$startDate = sections[$i]['startDate']
			$endDate = sections[$i]['endDate']
			
			$enrollment = sections[$i]['enrollment']
			$enrolled = $enrollment.split("/")[0]
			$capacity = $enrollment.split("/")[1]

			$days = sections[$i]['days']
			$courseTimes = sections[$i]['time']

			$inputStart = $courseTimes.split("-")[0]
			if $inputStart.include?("TBA")
				$startTime = "TBA"
				$formattedStartTime = "TBA"
			else			
                        	$startTime = $courseTimes.split("-")[0].insert(2,':')
				$formattedStartTime = Time.parse($startTime).strftime("%l:%M%P")
			end

			$inputEnd = $courseTimes.split("-")[1]
			if $inputEnd.include?("TBA")
				$endTime = "TBA"
				$formattedEndTime = "TBA"
			else
                        	$endTime = $courseTimes.split("-")[1].insert(2,':')
                        	$formattedEndTime = Time.parse($endTime).strftime("%l:%M%P")
			end

			$location = sections[$i]['location']			
			
			#puts "#{$startTime} #{$formattedStartTime} \t\t #{$endTime} #{$formattedEndTime}"
			if Section.exists?(:crn => $crn)
				puts "SECTION ALREADY EXISTS"
			else
				@section = Section.new(
					:sectionNum => $section,
					:crn => $crn,
					:courseType => $courseType,
					:startDate => $startDate,
					:endDate => $endDate,
					:capacity => $capacity,
					:enrolled => $enrolled,
					:days => $days,
					:startTime => $startTime,
					:endTime => $endTime,
					:location => $location,
					:instructor_id => $instructor_id,
					:course_id => @course.id,
					:prettyStartTime => $formattedStartTime,
					:prettyEndTime => $formattedEndTime
				)
				if @section.save
					puts "SECTION: CRN: #{$crn} \t Time: #{$formattedStartTime} to #{$formattedEndTime} \tCourse: #{@course.title}"
				else
					puts "ERROR LOADING SECTION"
				end
			end
			$i +=1
		end
	end	


	def converttoTimeString(input)
		$military = input.split('',4)
		if input.include?("TBA")
			$time = 'TBA'
		elsif $military[0..1].join().to_i > 12
 			$hours = $military[0..1].join().to_i - 12
 			$time = $hours.to_s + ":" + $military[2..3].join() + "pm"
		else
			$time = $military[1] + ":" + $military[2..3].join() + "am"
		end
		return $time
	end
end
