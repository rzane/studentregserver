////////////////////////// SECTIONS
	$('#listsections').live('pageshow', function (event, ui) {
		$.mobile.loading('show');
		$.ajax({
	  	url: "http://localhost:3000/sections?course_id=2832"
		}).done(function ( data ) {
		  var coursename = data[0]['section']['course']['title'];
		  $('#coursename').text(coursename);
			addSectionDatatoList(data);
		});
  });

function addSectionDatatoList (data) {
	for(var i = 0; i < data.length; i++) {
    	var sectionNum = data[i]['section']['sectionNum'];
			var link = $('<a>').text(sectionNum);
			var listitem = $('<li>').append(link);
    	$('#sectionlist').append(listitem);
 	}
	$('#sectionlist').listview('refresh');
	$.mobile.loading('hide');
}

///////////////////////////////////////////////////////////////
////////////////  COURSES  //////////////////////////////////////
///////////////////////////////////////////////////////////////
	$('#courses').live('pageshow', function (event, ui) {
		$.mobile.loading('show');
		$.ajax({
	  	url: "http://10.0.0.37:3000/courses.json?page=1"
		}).done(function ( data ) {
			addCourseDatatoList(data);
		});
		
		var page = 1;
		$(window).scroll(function()
		{
				var nearToBottom = 100;
				//if($(window).scrollTop() + $(window).height() > $(document).height() - 75)
				//if($(window).scrollTop() == $(document).height() - $(window).height())
				if ($(window).scrollTop() + $(window).height() > $(document).height() - nearToBottom)
		    {
							page += 1;
		        	$.ajax({
		        		url: "http://10.0.0.37:3000/courses.json?page=" + page
							}).done(function ( data ) {
								addCourseDatatoList(data);
		        	});
		    }
		});
  });

function addCourseDatatoList (data) {
	for(var i = 0; i < data.length; i++) {
    	var coursetitle = data[i]['course']['title'];
			var deptid = data[i]['course']['department_id'];
			var deptname = data[i]['course']['department']['name'];
    	//console.log(coursetitle);
			var link = $('<a>').text(coursetitle);
			var listitem = $('<li>').attr({'data-department-id':deptid, 'data-department-name':deptname}).append(link);
    	$('#courselist').append(listitem);
 	}
	//$('#courselist').listview('refresh');
	$('#courselist').listview({
			autodividers : true,
			autodividersSelector: function(li) {
				var out = li.attr('data-department-name');
				console.log(out);
				return out;
			}
		}).listview('refresh');
	$.mobile.loading('hide');
}
