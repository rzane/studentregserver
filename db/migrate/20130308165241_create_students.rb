class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :firstname
      t.string :lastname
      t.integer :studentIDnumber
      t.string :email
      t.string :password
      t.integer :searchable
      t.integer :registrationTimeID

      t.timestamps
    end
  end
end
