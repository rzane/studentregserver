class FixSectionCourseIdName < ActiveRecord::Migration
  def change
		rename_column :sections, :courseID, :course_id
    rename_column :sections, :instructorID, :instructor_id
  end
end
