class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :subject
      t.integer :courseNum
      t.string :title
      t.integer :credits
      t.integer :departmentID
      t.integer :termID

      t.timestamps
    end
  end
end
