class AddPasswordHashAndPasswordSaltToStudents < ActiveRecord::Migration
  def change
    add_column :students, :password_hash, :string
    add_column :students, :password_salt, :string
  end
end
