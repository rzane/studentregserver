class CreateRegtimes < ActiveRecord::Migration
  def change
    create_table :regtimes do |t|
      t.string :studenttype
      t.datetime :startTime
      t.integer :termID

      t.timestamps
    end
  end
end
