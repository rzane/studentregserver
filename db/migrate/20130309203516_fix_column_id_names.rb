class FixColumnIdNames < ActiveRecord::Migration
	def change
		#rename_column(:sections, :courseID, :course_id)
		rename_column(:courses, :termID, :term_id)
		rename_column(:students, :registrationTimeID, :regtime_id)
		rename_column(:tempregs, :studentID, :student_id)
		rename_column(:tempregs, :sectionID, :section_id)
		rename_column(:instructors, :departmentID, :department_id)
		rename_column(:regtimes, :termID, :term_id)
	end
end
