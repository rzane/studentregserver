class CreateRegistrations < ActiveRecord::Migration
  def change
    create_table :registrations do |t|
      t.integer :studentID
      t.integer :sectionID

      t.timestamps
    end
  end
end
