class CreateTempregs < ActiveRecord::Migration
  def change
    create_table :tempregs do |t|
      t.integer :studentID
      t.integer :sectionID

      t.timestamps
    end
  end
end
