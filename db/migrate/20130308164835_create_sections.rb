class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.integer :crn
      t.string :courseType
      t.datetime :startDate
      t.datetime :endDate
      t.integer :capacity
      t.integer :enrolled
      t.string :days
      t.time :startTime
      t.time :endTime
      t.string :location
      t.integer :instructorID
      t.integer :courseID

      t.timestamps
    end
  end
end
