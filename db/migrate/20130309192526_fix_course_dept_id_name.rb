class FixCourseDeptIdName < ActiveRecord::Migration
	def change
		rename_column :courses, :departmentID, :department_id
	end
end
