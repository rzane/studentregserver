class AddPrettyStartTimeAndPrettyEndTimeToSections < ActiveRecord::Migration
  def change
    add_column :sections, :prettyStartTime, :string
    add_column :sections, :prettyEndTime, :string
  end
end
