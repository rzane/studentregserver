class CreateInstructors < ActiveRecord::Migration
  def change
    create_table :instructors do |t|
      t.string :firstname
      t.string :lastname
      t.string :rmpUrl
      t.integer :departmentID

      t.timestamps
    end
  end
end
