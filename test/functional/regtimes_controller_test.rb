require 'test_helper'

class RegtimesControllerTest < ActionController::TestCase
  setup do
    @regtime = regtimes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:regtimes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create regtime" do
    assert_difference('Regtime.count') do
      post :create, regtime: { startTime: @regtime.startTime, studenttype: @regtime.studenttype, termID: @regtime.termID }
    end

    assert_response 201
  end

  test "should show regtime" do
    get :show, id: @regtime
    assert_response :success
  end

  test "should update regtime" do
    put :update, id: @regtime, regtime: { startTime: @regtime.startTime, studenttype: @regtime.studenttype, termID: @regtime.termID }
    assert_response 204
  end

  test "should destroy regtime" do
    assert_difference('Regtime.count', -1) do
      delete :destroy, id: @regtime
    end

    assert_response 204
  end
end
