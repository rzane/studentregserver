require 'test_helper'

class SectionsControllerTest < ActionController::TestCase
  setup do
    @section = sections(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create section" do
    assert_difference('Section.count') do
      post :create, section: { capacity: @section.capacity, courseID: @section.courseID, courseType: @section.courseType, crn: @section.crn, days: @section.days, endDate: @section.endDate, endTime: @section.endTime, enrolled: @section.enrolled, instructorID: @section.instructorID, location: @section.location, startDate: @section.startDate, startTime: @section.startTime }
    end

    assert_response 201
  end

  test "should show section" do
    get :show, id: @section
    assert_response :success
  end

  test "should update section" do
    put :update, id: @section, section: { capacity: @section.capacity, courseID: @section.courseID, courseType: @section.courseType, crn: @section.crn, days: @section.days, endDate: @section.endDate, endTime: @section.endTime, enrolled: @section.enrolled, instructorID: @section.instructorID, location: @section.location, startDate: @section.startDate, startTime: @section.startTime }
    assert_response 204
  end

  test "should destroy section" do
    assert_difference('Section.count', -1) do
      delete :destroy, id: @section
    end

    assert_response 204
  end
end
