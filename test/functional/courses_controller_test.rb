require 'test_helper'

class CoursesControllerTest < ActionController::TestCase
  setup do
    @course = courses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:courses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create course" do
    assert_difference('Course.count') do
      post :create, course: { courseNum: @course.courseNum, credits: @course.credits, departmentID: @course.departmentID, subject: @course.subject, termID: @course.termID, title: @course.title }
    end

    assert_response 201
  end

  test "should show course" do
    get :show, id: @course
    assert_response :success
  end

  test "should update course" do
    put :update, id: @course, course: { courseNum: @course.courseNum, credits: @course.credits, departmentID: @course.departmentID, subject: @course.subject, termID: @course.termID, title: @course.title }
    assert_response 204
  end

  test "should destroy course" do
    assert_difference('Course.count', -1) do
      delete :destroy, id: @course
    end

    assert_response 204
  end
end
