require 'test_helper'

class TermsControllerTest < ActionController::TestCase
  setup do
    @term = terms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:terms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create term" do
    assert_difference('Term.count') do
      post :create, term: { name: @term.name }
    end

    assert_response 201
  end

  test "should show term" do
    get :show, id: @term
    assert_response :success
  end

  test "should update term" do
    put :update, id: @term, term: { name: @term.name }
    assert_response 204
  end

  test "should destroy term" do
    assert_difference('Term.count', -1) do
      delete :destroy, id: @term
    end

    assert_response 204
  end
end
