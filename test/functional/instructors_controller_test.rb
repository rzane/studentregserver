require 'test_helper'

class InstructorsControllerTest < ActionController::TestCase
  setup do
    @instructor = instructors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:instructors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create instructor" do
    assert_difference('Instructor.count') do
      post :create, instructor: { departmentID: @instructor.departmentID, firstname: @instructor.firstname, lastname: @instructor.lastname, rmpUrl: @instructor.rmpUrl }
    end

    assert_response 201
  end

  test "should show instructor" do
    get :show, id: @instructor
    assert_response :success
  end

  test "should update instructor" do
    put :update, id: @instructor, instructor: { departmentID: @instructor.departmentID, firstname: @instructor.firstname, lastname: @instructor.lastname, rmpUrl: @instructor.rmpUrl }
    assert_response 204
  end

  test "should destroy instructor" do
    assert_difference('Instructor.count', -1) do
      delete :destroy, id: @instructor
    end

    assert_response 204
  end
end
