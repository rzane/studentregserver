require 'test_helper'

class TempregsControllerTest < ActionController::TestCase
  setup do
    @tempreg = tempregs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tempregs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tempreg" do
    assert_difference('Tempreg.count') do
      post :create, tempreg: { sectionID: @tempreg.sectionID, studentID: @tempreg.studentID }
    end

    assert_response 201
  end

  test "should show tempreg" do
    get :show, id: @tempreg
    assert_response :success
  end

  test "should update tempreg" do
    put :update, id: @tempreg, tempreg: { sectionID: @tempreg.sectionID, studentID: @tempreg.studentID }
    assert_response 204
  end

  test "should destroy tempreg" do
    assert_difference('Tempreg.count', -1) do
      delete :destroy, id: @tempreg
    end

    assert_response 204
  end
end
